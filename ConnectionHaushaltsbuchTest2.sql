create table if not exists  "Eintrage2" (
                            "ID" Integer Not Null,
                            "Kategorie" text not null,
                            "Betrag" double not null,
                            "EinmaligPeriodisch" integer not null, 
                            "PositivNegativ" integer not null,
                            "Datum" date not null
                            )

insert into "Eintrage2" values (1, 'Miete', '10,00', 1, 0, '03.10.2020');
insert into "Eintrage2" values (2, '450,00', 1, 1, '03.10.2020');
insert into "Eintrage2" values (3, '30,00', 1, 0, '07.10.2020');
insert into "Eintrage2" values (4, '500,00', 1, 1, '07.10.2020');

select * from Eintrage2;

drop table Eintrage2;