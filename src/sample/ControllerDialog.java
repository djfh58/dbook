package sample;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

public class ControllerDialog implements Initializable {
    @FXML
    JFXButton btnNegativerEintrag;
    @FXML
    JFXButton btnPositiverEintrag;
    @FXML
    JFXButton btnDetails;
    @FXML
    Label lblNKontostand;
    @FXML
    Label lblAKontostand;
    @FXML
    Label lblEintrag;

    public double sumP = 0.0;
    public double sumN = 0.0;
    public double summe = 0.0;
    public double dnum;
    boolean dark;
    int id;

    public ControllerDialog() throws IOException {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        )
        {String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while(rs.next()){
                if(rs.getInt(1)==1){
                    this.dark=true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark=false;
                    System.out.println(rs.getInt(1));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();

        }
    }

    public void positiverEintragErfassen() throws IOException {
        id=6;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnPositiverEintrag.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void negativerEintragErfassen() throws IOException {
        id=2;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnNegativerEintrag.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Details() throws IOException {
        id=4;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnDetails.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Parent getParent(int id) throws IOException {
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent parent1 = fxmlLoader1.load();
        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("Ausgaben_Erfassen.fxml"));
        Parent parent2 = fxmlLoader2.load();
        FXMLLoader fxmlLoader3 = new FXMLLoader(getClass().getResource("Datenschutz.fxml"));
        Parent parent3 = fxmlLoader3.load();
        FXMLLoader fxmlLoader4 = new FXMLLoader(getClass().getResource("Detailansicht.fxml"));
        Parent parent4 = fxmlLoader4.load();
        FXMLLoader fxmlLoader5 = new FXMLLoader(getClass().getResource("Dialog.fxml"));
        Parent parent5 = fxmlLoader5.load();
        FXMLLoader fxmlLoader6 = new FXMLLoader(getClass().getResource("Einnahmen_Erfassen.fxml"));
        Parent parent6 = fxmlLoader6.load();
        FXMLLoader fxmlLoader7 = new FXMLLoader(getClass().getResource("Impressum.fxml"));
        Parent parent7 = fxmlLoader7.load();
        FXMLLoader fxmlLoader8 = new FXMLLoader(getClass().getResource("ÜberUns.fxml"));
        Parent parent8 = fxmlLoader8.load();

        // fxmlLoader9


        if(dark==true){
            parent1.setStyle("-fx-background-color: #A9A9A9");
            parent2.setStyle("-fx-background-color: #A9A9A9");
            parent3.setStyle("-fx-background-color: #A9A9A9");
            parent4.setStyle("-fx-background-color: #A9A9A9");
            parent5.setStyle("-fx-background-color: #A9A9A9");
            parent6.setStyle("-fx-background-color: #A9A9A9");
            parent7.setStyle("-fx-background-color: #A9A9A9");
            parent8.setStyle("-fx-background-color: #A9A9A9");
        }

        switch (id){
            case 1:
                return parent1;
            case 2:
                return parent2;
            case 3:
                return parent3;
            case 4:
                return parent4;
            case 5:
                return parent5;
            case 6:
                return parent6;
            case 7:
                return parent7;
            case 8:
                return parent8;
        }
        return parent1;
    }

    public void getDnumN(double dnum){
        DecimalFormat df = new DecimalFormat("#.00");
        lblEintrag.setText("-" + df.format(dnum) + " €");
        summe = summe + dnum;
        if (summe == 0.0) {
            lblAKontostand.setText("0,00 €");
        } else {
            lblAKontostand.setText(df.format(summe));
        }
    }

    public void getDnumP(double dnum){
        DecimalFormat df = new DecimalFormat("#.00");
        lblEintrag.setText("+" + df.format(dnum) + " €");
        summe = summe - dnum;
        if (summe == 0.0){
            lblAKontostand.setText("0,00 €");
        } else {
            lblAKontostand.setText(df.format(summe));
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        //ControllerAusgaben con = new ControllerAusgaben();
        //dnum = con.getDnum();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 0";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");
            while (resP.next()) {
                Double c = resP.getDouble(1);
                sumP = sumP + c;
                System.out.println(sumP);

            }

            String sqlSelectN = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 1";
            ResultSet resN = stmt.executeQuery(sqlSelectN);
            System.out.println("The SQL statement is: " + sqlSelectN + "\n");
            while (resN.next()) {
                Double c = resN.getDouble(1);
                sumN = sumN + c;
                System.out.println(sumN);
            }
            summe = sumP - sumN;

            DecimalFormat df = new DecimalFormat("#.00");
            lblNKontostand.setText(df.format(summe) + " €");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
