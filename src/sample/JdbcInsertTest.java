package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.Date;


public class JdbcInsertTest {
    public static void main(String[] args) {
    }

    public void DbDrop() {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlInsert = "Drop Table Eintrage";
            System.out.println("The SQL statement is: " + sqlInsert + "\n");  // Echo for debugging
            int countInserted = stmt.executeUpdate(sqlInsert);
            System.out.println(countInserted + " records inserted.\n");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void DbDropMetaData() {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlInsert = "Drop Table MetaData";
            System.out.println("The SQL statement is: " + sqlInsert + "\n");  // Echo for debugging
            int countInserted = stmt.executeUpdate(sqlInsert);
            System.out.println(countInserted + " records inserted.\n");
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void Reset() {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {

            String sqlReset = "Delete From Eintrage";
            System.out.println("The SQL statement is: " + sqlReset + "\n");  // Echo for debugging
            int countInserted = stmt.executeUpdate(sqlReset);
            System.out.println(countInserted + " records inserted.\n");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    public void DbInsert(String Kategorie, Double Betrag, Integer EinmaligPeriodisch, Integer PositivNegativ, LocalDate Datum) {
        int i = 0;
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {

            if(EinmaligPeriodisch == 0) { // Einmaliger Eintrag
                String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                System.out.println("The SQL statement is: " + sqlInsert + "\n");  // Echo for debugging
                int countInserted = stmt.executeUpdate(sqlInsert);
                System.out.println(countInserted + " records inserted.\n");
            } else if(EinmaligPeriodisch == 1) { // Wöchentlich Eintrag
                while (i < 4) {
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    Datum = Datum.plusDays(7);
                    i++;
                }
            } else if(EinmaligPeriodisch == 2) { // Monatlich Eintrag
                while (i < 4) {
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    Datum = Datum.plusMonths(1);
                    i++;
                }
            } else if (EinmaligPeriodisch == 3) { // Quartalsweiser Eintrag
                while (i < 4) {
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    Datum = Datum.plusMonths(3);
                    i++;
                }
            } else if (EinmaligPeriodisch == 4) { // Jährlicher Eintrag
                while (i < 4) {
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    Datum = Datum.plusYears(1);
                    i++;
                }
            }

            String strSelect = "select * from Eintrage";
            System.out.println("The SQL statement is: " + strSelect + "\n");  // Echo For debugging
            ResultSet rset = stmt.executeQuery(strSelect);
            while (rset.next()) {   // Move the cursor to the next row
                System.out.println(rset.getInt("ID") + ", "
                        + rset.getString("Kategorie") + ", "
                        + rset.getString("Betrag") + ", "
                        + rset.getString("EinmaligPeriodisch") + ", "
                        + rset.getDouble("PositivNegativ") + ", "
                        + rset.getString("Datum"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    public void DbSetup() {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {

            String sqlCreate = "create table if not exists  \"Eintrage\" (\n" +
                    "                            \"ID\" Integer PRIMARY KEY, \n" +
                    "                            \"Kategorie\" text Not Null, \n" +
                    "                            \"Betrag\" double not null,\n" +
                    "                            \"EinmaligPeriodisch\" integer not null, \n" +
                    "                            \"PositivNegativ\" integer not null,\n" +
                    "                            \"Datum\" DATE not null\n" +
                    "                            )";

            System.out.println("The SQL statement is: " + sqlCreate + "\n");
            stmt.executeUpdate(sqlCreate);

            /*
            String strSelect = "select * from Eintrage";
            System.out.println("The SQL statement is: " + strSelect + "\n");  // Echo For debugging
            ResultSet rset = stmt.executeQuery(strSelect);
            while (rset.next()) {   // Move the cursor to the next row
                System.out.println(rset.getInt("ID") + ", "
                        + rset.getString("Betrag") + ", "
                        + rset.getString("EinmaligPeriodisch") + ", "
                        + rset.getDouble("PositivNegativ") + ", "
                        + rset.getString("Datum"));
            }

             */
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void DBSetupMetaData() {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlCreate = "create table if not exists  \"MetaData\" (\n" +
                    "                            \"ID\" Integer PRIMARY KEY, \n" +
                    "                            \"Darkmode\" Integer)";

            System.out.println("The SQL statement is: " + sqlCreate + "\n");
            stmt.executeUpdate(sqlCreate);

            String sqlCreate2 = "Insert into MetaData(ID, Darkmode) Values (1, 0)";
            System.out.println("The SQL Statement is: " + sqlCreate2);
            stmt.executeUpdate(sqlCreate2);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void FormatDate(){
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String dateformat = "FORMAT(Datum, 'dd.MM.yyyy')";
            System.out.println("The SQL statement is: " + dateformat + "\n");

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void Selectall(){
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String selectall = "Select * From Eintrage";
            ResultSet rset = stmt.executeQuery(selectall);
            System.out.println("The SQL statement is: " + selectall + "\n");
            while (rset.next()) {   // Move the cursor to the next row
                System.out.println(rset.getInt("ID") + ", "
                        + rset.getString("Betrag") + ", "
                        + rset.getString("EinmaligPeriodisch") + ", "
                        + rset.getDouble("PositivNegativ") + ", "
                        + rset.getInt("Datum"));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public static void SelectallMetaData(){
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String selectall = "Select * From MetaData";
            ResultSet rset = stmt.executeQuery(selectall);
            System.out.println("The SQL statement is: " + selectall + "\n");
            while (rset.next()) {   // Move the cursor to the next row
                System.out.println(rset.getInt("ID") + ", "
                        + rset.getString("Darkmode"));
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }


    @FXML
    public String DbZeigeKontostand() {
        Double sumP = 0.0;
        Double sumN = 0.0;

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 0";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");
            while (resP.next()){
               Double c = resP.getDouble(1);
               sumP = sumP + c;
               System.out.println(sumP);
           }

            //String label;
            String label = String.valueOf(sumP);

            System.out.println(label);

            return label;
            // lblKontostand.setText(sumP);
/*
            String sqlSelectN = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 1";
            ResultSet resN = stmt.executeQuery(sqlSelectN);
            System.out.println("The SQL statement is: " + sqlSelectN + "\n");
            while (resN.next()){
                Double c = resN.getDouble(1);
                sumN = sumN + c;
                System.out.println(sumN);
                // lblKontostand.setText(String.valueOf(sum));
            }

 */
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveDarkmode(boolean dark){
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            if (dark == true) {
                String sqlupdate = "Update MetaData Set Darkmode = 1 Where ID = 1";
                stmt.executeUpdate(sqlupdate);
            } if (dark == false) {
                String sqlupdate = "Update MetaData Set Darkmode = 0 Where ID = 1";
                stmt.executeUpdate(sqlupdate);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Die Darkvariable hat den Wert:" + dark);
    }

    public ArrayList<String> readCategory(){
        ArrayList<String> kategorie = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Kategorie from Eintrage Order By Datum DESC";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getString("Kategorie"));
                kategorie.add(resP.getString("Kategorie"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public ArrayList<String> readCategoryEinnahmen(){
        ArrayList<String> kategorie = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Kategorie from Eintrage where PositivNegativ == 0";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getString("Kategorie"));
                kategorie.add(resP.getString("Kategorie"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public ArrayList<String> readCategoryAusgaben(){
        ArrayList<String> kategorie = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Kategorie from Eintrage where PositivNegativ == 1";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getString("Kategorie"));
                kategorie.add(resP.getString("Kategorie"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public ArrayList<Double> readBetrag(){
        ArrayList<Double> kategorie = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Betrag, PositivNegativ from Eintrage Order By Datum DESC";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                if (resP.getDouble("PositivNegativ") == 0) {
                    System.out.println(resP.getDouble("Betrag"));
                    kategorie.add(resP.getDouble("Betrag"));
                } else if (resP.getDouble("PositivNegativ") == 1) {
                    System.out.println(resP.getDouble("Betrag"));
                    kategorie.add(resP.getDouble("Betrag") * -1);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public ArrayList<Double> readBetragEinnahmen(){
        ArrayList<Double> kategorie = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Betrag from Eintrage where PositivNegativ == 0";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getDouble("Betrag"));
                kategorie.add(resP.getDouble("Betrag"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public ArrayList<Double> readBetragAusgaben(){
        ArrayList<Double> kategorie = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Betrag from Eintrage where PositivNegativ == 1";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getDouble("Betrag"));
                kategorie.add(resP.getDouble("Betrag"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public ArrayList<Double> readSumBetrag() {
        Double sumP = 0.0;
        Double sumN = 0.0;
        ArrayList<Double> arraybetrag = new ArrayList<>();
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 0";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");
            while (resP.next()) {
                Double c = resP.getDouble(1);
                sumP = sumP + c;
                System.out.println(sumP);
            }

            String sqlSelectN = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 1";
            ResultSet resN = stmt.executeQuery(sqlSelectN);
            System.out.println("The SQL statement is: " + sqlSelectN + "\n");
            while (resN.next()) {
                Double c = resN.getDouble(1);
                sumN = sumN + c;
                System.out.println(sumN);
            }
            arraybetrag.add(sumP);
            arraybetrag.add(sumN);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return arraybetrag;
    }

    public ArrayList<Integer> readIndexAusgaben(){
        ArrayList<Integer> ID = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT ID from Eintrage where PositivNegativ == 1";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getInt("ID"));
                ID.add(resP.getInt("ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ID;
    }

    public ArrayList<Integer> readIndexEinnahmen(){
        ArrayList<Integer> ID = new ArrayList<>();

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT ID from Eintrage where PositivNegativ == 0";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            while (resP.next()){
                System.out.println(resP.getInt("ID"));
                ID.add(resP.getInt("ID"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ID;
    }

    public double readBetragIndex(int index){
        double betrag = 0;
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Betrag from Eintrage where ID ==" + index;
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            betrag = resP.getDouble("Betrag");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return betrag;
    }

    public String readKategorieIndex(int index){
        String kategorie = "";
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Kategorie from Eintrage where ID ==" + index;
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            kategorie = resP.getString("Kategorie");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return kategorie;
    }

    public LocalDate readDateIndex(int index){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse("2020-01-01", formatter);
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Datum from Eintrage where ID ==" + index;
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            date = LocalDate.parse(resP.getString("Datum"),formatter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return date;
    }

    public int readPositivNegativIndex(int index){
        int einnahme = 0;
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT PositivNegativ from Eintrage where ID ==" + index;
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            einnahme = resP.getInt("PositivNegativ");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return einnahme;
    }

    public int readEinmaligPeriodischIndex(int index){
        int einmalig = 0;
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT EinmaligPeriodisch from Eintrage where ID ==" + index;
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");

            einmalig = resP.getInt("EinmaligPeriodisch");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Einmalig periodisch: "+einmalig);
        return einmalig;

    }

    public double analyseEinnahmen(){
        int i=0;
        double einnahmen = 0.0;
        double gehalt = 0.0;
        String heute;
        String letzteWoche;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Calendar c = Calendar.getInstance();
        heute = formatter.format(c.getTime());

        c.add(Calendar.DAY_OF_MONTH, -7);
        letzteWoche = formatter.format(c.getTime());

        System.out.println("heute ist der "+heute+ " und letzte woche war der "+letzteWoche);

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "SELECT Betrag from Eintrage where Kategorie = 'Gehalt' order by Datum desc";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            while (resP.next()&&i==0) {
               i++;
               gehalt = resP.getDouble("Betrag")/4;
            }
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");


            //zweite Abfrage: Einnahmen der letzten Woche


            String sqlSelectP1 = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 0 and Datum <= '" + heute + "'and Datum >= '" + letzteWoche+"' and Kategorie != 'Gehalt'";
            ResultSet resP1 = stmt.executeQuery(sqlSelectP1);
            System.out.println("The SQL statement is: " + sqlSelectP1 + "\n");
            einnahmen = gehalt + resP1.getDouble("SUM(Betrag)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return einnahmen;
    }

    public double analyseAusgaben(){
        String heute;
        String letzteWoche;
        double ausgaben = 0.0;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Calendar c = Calendar.getInstance();
        heute = formatter.format(c.getTime());

        c.add(Calendar.DAY_OF_MONTH, -7);
        letzteWoche = formatter.format(c.getTime());
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 1 and Datum <= '" + heute + "'and Datum >= '" + letzteWoche+"'";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");
            ausgaben = resP.getDouble("SUM(Betrag)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ausgaben;
    }

    public double analyseKategorieAusgaben(String kategorie){
        String heute;
        String letzteWoche;
        double ausgaben = 0.0;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        heute = formatter.format(c.getTime());

        c.add(Calendar.DAY_OF_MONTH, -7);
        letzteWoche = formatter.format(c.getTime());
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "Select SUM(Betrag) from Eintrage Where PositivNegativ = 1 and Datum <= '" + heute + "'and Datum >= '" + letzteWoche+"' and Kategorie = '"+kategorie+"'";
            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");
            ausgaben = resP.getDouble("SUM(Betrag)");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ausgaben;
    }

    public void DbInsertBearbeiten(String Kategorie, Double Betrag, Integer EinmaligPeriodisch, Integer PositivNegativ, LocalDate Datum) {
        int i = 0;
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {

            if(EinmaligPeriodisch == 1) { // Wöchentlich Eintrag
                while (i < 3) {
                    Datum = Datum.plusDays(7);
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    i++;
                }
            } else if(EinmaligPeriodisch == 2) { // Monatlich Eintrag
                while (i < 3) {
                    Datum = Datum.plusMonths(1);
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    i++;
                }
            } else if (EinmaligPeriodisch == 3) { // Quartalsweiser Eintrag
                while (i < 3) {
                    Datum = Datum.plusMonths(3);
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    i++;
                }
            } else if (EinmaligPeriodisch == 4) { // Jährlicher Eintrag
                while (i < 3) {
                    Datum = Datum.plusYears(1);
                    String sqlInsert = "insert into Eintrage values (Null, '" + Kategorie + "', '" + Betrag + "', " + EinmaligPeriodisch + ", " + PositivNegativ + ", '" + Datum + "')";
                    stmt.executeUpdate(sqlInsert);
                    i++;
                }
            }

            String strSelect = "select * from Eintrage";
            System.out.println("The SQL statement is: " + strSelect + "\n");  // Echo For debugging
            ResultSet rset = stmt.executeQuery(strSelect);
            while (rset.next()) {   // Move the cursor to the next row
                System.out.println(rset.getInt("ID") + ", "
                        + rset.getString("Kategorie") + ", "
                        + rset.getString("Betrag") + ", "
                        + rset.getString("EinmaligPeriodisch") + ", "
                        + rset.getDouble("PositivNegativ") + ", "
                        + rset.getString("Datum"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
