package sample;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Item {
    private final SimpleStringProperty Kategorie;
    private final SimpleDoubleProperty Betrag;

    public Item(String kateg, Double betrag) {
        this.Kategorie = new SimpleStringProperty(kateg);
        this.Betrag = new SimpleDoubleProperty(betrag);
    }

    public void setKategorie(String kategorie) {
        Kategorie.set(kategorie);
    }

    public void setBetrag(Double betrag) {
        Betrag.set(betrag);
    }

    public String getKategorie() {
        return Kategorie.get();
    }

    public Double getBetrag() {
        return Betrag.get();
    }
}
