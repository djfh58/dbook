package sample;

import com.jfoenix.controls.JFXButton;
import javafx.application.Application;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ControllerAusgaben implements Initializable {
    @FXML
    Label impressum;
    @FXML
    Label datenschutz;
    @FXML
    Label überuns;
    @FXML
    ImageView book;
    @FXML
    JFXButton btnPositiverEintrag;
    @FXML
    JFXButton speichern;
    @FXML
    ToggleGroup Kategorie;
    @FXML
    DatePicker datePicker;
    @FXML
    TextField tfBetrag;
    @FXML
    ToggleButton tbEinmaligPeriodisch;
    @FXML
    ComboBox periode;
    @FXML
    Label warning;

    FXMLLoader fxmlLoader5;

    public double dnum;
    boolean dark;
    int id;

    public ControllerAusgaben() throws IOException {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        )
        {String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while(rs.next()){
                if(rs.getInt(1)==1){
                    this.dark=true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark=false;
                    System.out.println(rs.getInt(1));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();

        }
    }

    public void home() throws IOException {
        id=1;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) book.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Impressum() throws IOException {
        id=7;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) impressum.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Datenschutz() throws IOException {
        id=3;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) datenschutz.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Überuns() throws IOException {
        id=8;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) überuns.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void positiverEintragErfassen() throws IOException {
        id=6;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnPositiverEintrag.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void speichern() throws IOException {
        id=5;

        int ep = 0;
        String tgValue = "";
        RadioButton selectedRadioButton = (RadioButton) Kategorie.getSelectedToggle();
        try {
            tgValue = selectedRadioButton.getText();
        } catch (Exception e) {
            warning.setVisible(true);
            return;
        }

        String betrag = tfBetrag.getText();
        if(betrag.equals("")) {
            warning.setText("Bitte Betragsfeld ausfüllen!");
            warning.setVisible(true);
            return;
        }

        dnum = Double.parseDouble(betrag);

        boolean EinmaligPeriodisch = tbEinmaligPeriodisch.isSelected();
        if (EinmaligPeriodisch == false){
            ep = 0;
            System.out.println(ep);
        } else {
            if(periode.getValue() == "Wöchentlich"){
                ep = 1;
            } else if (periode.getValue() == "Monatlich") {
                ep = 2;
            } else if (periode.getValue() == "Quartalsweise") {
                ep = 3;
            } else if (periode.getValue() == "Jährlich") {
                ep = 4;
            }
            System.out.println("Periodisch ausgewählt: "+ep);
        }

        LocalDate date = datePicker.getValue();
        if(date == null){
            warning.setText("Bitte Datum auswählen!");
            warning.setVisible(true);
            return;
        }

        JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
        jdbcInsertTest.DbInsert(tgValue, dnum, ep, 1, date);


        Scene scene = new Scene(getParent(id), 600, 400);
        try{
            ((Stage) speichern.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ControllerDialog controllerDialog = fxmlLoader5.getController();
        controllerDialog.getDnumN(dnum);
    }

    public void warning(){
        warning.setVisible(true);
    }


    public Parent getParent(int id) throws IOException {
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent parent1 = fxmlLoader1.load();
        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("Ausgaben_Erfassen.fxml"));
        Parent parent2 = fxmlLoader2.load();
        FXMLLoader fxmlLoader3 = new FXMLLoader(getClass().getResource("Datenschutz.fxml"));
        Parent parent3 = fxmlLoader3.load();
        FXMLLoader fxmlLoader4 = new FXMLLoader(getClass().getResource("Detailansicht.fxml"));
        Parent parent4 = fxmlLoader4.load();
        fxmlLoader5 = new FXMLLoader(getClass().getResource("Dialog.fxml"));
        Parent parent5 = fxmlLoader5.load();
        FXMLLoader fxmlLoader6 = new FXMLLoader(getClass().getResource("Einnahmen_Erfassen.fxml"));
        Parent parent6 = fxmlLoader6.load();
        FXMLLoader fxmlLoader7 = new FXMLLoader(getClass().getResource("Impressum.fxml"));
        Parent parent7 = fxmlLoader7.load();
        FXMLLoader fxmlLoader8 = new FXMLLoader(getClass().getResource("ÜberUns.fxml"));
        Parent parent8 = fxmlLoader8.load();

        if(dark==true){
            parent1.setStyle("-fx-background-color: #A9A9A9");
            parent2.setStyle("-fx-background-color: #A9A9A9");
            parent3.setStyle("-fx-background-color: #A9A9A9");
            parent4.setStyle("-fx-background-color: #A9A9A9");
            parent5.setStyle("-fx-background-color: #A9A9A9");
            parent6.setStyle("-fx-background-color: #A9A9A9");
            parent7.setStyle("-fx-background-color: #A9A9A9");
            parent8.setStyle("-fx-background-color: #A9A9A9");
        }

        switch (id){
            case 1:
                return parent1;
            case 2:
                return parent2;
            case 3:
                return parent3;
            case 4:
                return parent4;
            case 5:
                return parent5;
            case 6:
                return parent6;
            case 7:
                return parent7;
            case 8:
                return parent8;
        }
        return parent1;
    }



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        warning.setVisible(false);
        //ComboBox mit Wählbarem Zeitraum wird gefüllt
        periode.setDisable(true);
        ObservableList<String> optionen = FXCollections.observableArrayList(
                "Wöchentlich",
                "Monatlich",
                "Quartalsweise",
                "Jährlich"
        );
        periode.setItems(optionen);
        periode.setValue("Zeitraum wählen");

        //Wenn der Eintrag als periodisch deklariert wird, wird die ComboBox für den Zeitraum sichtbar
        InvalidationListener listen = new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                if(tbEinmaligPeriodisch.isSelected()==true){
                    periode.setDisable(false);
                } else {
                    periode.setDisable(true);
                }
            }
        };
        tbEinmaligPeriodisch.selectedProperty().addListener(listen);

        FileInputStream stream = null;
        try {
            stream = new FileInputStream("src/Assets/book.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image img = new Image(stream);
        book.setImage(img);
    }
}
