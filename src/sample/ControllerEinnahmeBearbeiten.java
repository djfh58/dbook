package sample;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXToggleButton;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

import static java.lang.String.valueOf;

public class ControllerEinnahmeBearbeiten implements Initializable {
    @FXML
    ImageView book;
    @FXML
    JFXButton speichern;
    @FXML
    JFXButton löschen;
    @FXML
    TextField tfBetrag;
    @FXML
    DatePicker datePicker;
    @FXML
    JFXToggleButton tbEinnahmeAusgabe;
    @FXML
    JFXToggleButton tbEinmaligPeriodisch;
    @FXML
    ComboBox periode;
    @FXML
    RadioButton tbGehalt;
    @FXML
    RadioButton tbMieteinnahmen;
    @FXML
    RadioButton tbVerkauf;
    @FXML
    RadioButton tbKindergeld;
    @FXML
    RadioButton tbZinsen;
    @FXML
    RadioButton tbGeschenk;
    @FXML
    RadioButton tbRente;
    @FXML
    RadioButton tbAktien;
    @FXML
    RadioButton tbDiebstahl;
    @FXML
    RadioButton tbSonstiges;
    @FXML
    ToggleGroup Kategorie;
    @FXML
    Label warning;

    boolean dark;
    int id;
    int index;
    double betrag;
    double bet;
    LocalDate datum;
    LocalDate date;
    boolean periodisch;
    int ep;
    String bet1;
    String kategorie;
    FXMLLoader fxmlLoader9;
    FXMLLoader fxmlLoader10;


    public ControllerEinnahmeBearbeiten() throws SQLException {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        )
        {String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while(rs.next()){
                if(rs.getInt(1)==1){
                    this.dark=true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark=false;
                    System.out.println(rs.getInt(1));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();

        }
    }

    public void home() throws IOException {
        id=1;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) book.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void eintragLöschen() throws IOException {
        id=4;


        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            // Code um bearbeitete Einträge zu speichern

            String sqlUpdate = "DELETE From Eintrage Where ID = " + index;
            System.out.println("The SQL statement is: " + sqlUpdate);
            stmt.executeUpdate(sqlUpdate);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try{
            Scene scene = new Scene(getParent(id), 1280, 960);
            ((Stage) löschen.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void speichern() throws IOException {
        id=4;

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            // Code um bearbeitete Einträge zu speichern

            RadioButton selectedRadioButton = (RadioButton) Kategorie.getSelectedToggle();
            String tgValue = selectedRadioButton.getText();

            bet1 = tfBetrag.getText();
            if(bet1.equals("")) {
                warning.setText("Bitte Betragsfeld ausfüllen!");
                warning.setVisible(true);
                return;
            }
            bet = Double.parseDouble(bet1);

            boolean einmaligPeriodisch = tbEinmaligPeriodisch.isSelected();
            date = datePicker.getValue();

            if (einmaligPeriodisch == false){
                ep = 0;
                String sqlUpdate = "Update Eintrage SET Kategorie = '" + tgValue + "', Betrag = '" + bet + "', EinmaligPeriodisch = '" + ep + "', PositivNegativ = '0'" + ", Datum = '" + date + "' Where ID = " + index;
                stmt.executeUpdate(sqlUpdate);
            } else {
                if(periode.getValue() == "Wöchentlich"){
                    ep = 1;
                    String sqlUpdate = "Update Eintrage SET Kategorie = '" + tgValue + "', Betrag = '" + bet + "', EinmaligPeriodisch = '" + ep + "', PositivNegativ = '0'" + ", Datum = '" + date + "' Where ID = " + index;
                    stmt.executeUpdate(sqlUpdate);
                    JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
                    jdbcInsertTest.DbInsertBearbeiten(tgValue, bet, ep, 0, date);
                } else if (periode.getValue() == "Monatlich") {
                    ep = 2;
                    String sqlUpdate = "Update Eintrage SET Kategorie = '" + tgValue + "', Betrag = '" + bet + "', EinmaligPeriodisch = '" + ep + "', PositivNegativ = '0'" + ", Datum = '" + date + "' Where ID = " + index;
                    stmt.executeUpdate(sqlUpdate);
                    JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
                    jdbcInsertTest.DbInsertBearbeiten(tgValue, bet, ep, 0, date);
                } else if (periode.getValue() == "Quartalsweise") {
                    ep = 3;
                    String sqlUpdate = "Update Eintrage SET Kategorie = '" + tgValue + "', Betrag = '" + bet + "', EinmaligPeriodisch = '" + ep + "', PositivNegativ = '0'" + ", Datum = '" + date + "' Where ID = " + index;
                    stmt.executeUpdate(sqlUpdate);
                    JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
                    jdbcInsertTest.DbInsertBearbeiten(tgValue, bet, ep, 0, date);
                } else if (periode.getValue() == "Jährlich") {
                    ep = 4;
                    String sqlUpdate = "Update Eintrage SET Kategorie = '" + tgValue + "', Betrag = '" + bet + "', EinmaligPeriodisch = '" + ep + "', PositivNegativ = '0'" + ", Datum = '" + date + "' Where ID = " + index;
                    stmt.executeUpdate(sqlUpdate);
                    JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
                    jdbcInsertTest.DbInsertBearbeiten(tgValue, bet, ep, 0, date);
                }
                System.out.println("Periodisch ausgewählt: "+ep);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        try{
            Scene scene = new Scene(getParent(id), 1280, 960);
            ((Stage) speichern.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Parent getParent(int id) throws IOException {
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent parent1 = fxmlLoader1.load();
        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("Ausgaben_Erfassen.fxml"));
        Parent parent2 = fxmlLoader2.load();
        FXMLLoader fxmlLoader3 = new FXMLLoader(getClass().getResource("Datenschutz.fxml"));
        Parent parent3 = fxmlLoader3.load();
        FXMLLoader fxmlLoader4 = new FXMLLoader(getClass().getResource("Detailansicht.fxml"));
        Parent parent4 = fxmlLoader4.load();
        FXMLLoader fxmlLoader5 = new FXMLLoader(getClass().getResource("Dialog.fxml"));
        Parent parent5 = fxmlLoader5.load();
        FXMLLoader fxmlLoader6 = new FXMLLoader(getClass().getResource("Einnahmen_Erfassen.fxml"));
        Parent parent6 = fxmlLoader6.load();
        FXMLLoader fxmlLoader7 = new FXMLLoader(getClass().getResource("Impressum.fxml"));
        Parent parent7 = fxmlLoader7.load();
        FXMLLoader fxmlLoader8 = new FXMLLoader(getClass().getResource("ÜberUns.fxml"));
        Parent parent8 = fxmlLoader8.load();
        fxmlLoader9 = new FXMLLoader(getClass().getResource("EinnahmeBearbeiten.fxml"));
        Parent parent9 = fxmlLoader9.load();
        fxmlLoader10 = new FXMLLoader(getClass().getResource("AusgabeBearbeiten.fxml"));
        Parent parent10 = fxmlLoader10.load();

        if(dark==true){
            parent1.setStyle("-fx-background-color: #A9A9A9");
            parent2.setStyle("-fx-background-color: #A9A9A9");
            parent3.setStyle("-fx-background-color: #A9A9A9");
            parent4.setStyle("-fx-background-color: #A9A9A9");
            parent5.setStyle("-fx-background-color: #A9A9A9");
            parent6.setStyle("-fx-background-color: #A9A9A9");
            parent7.setStyle("-fx-background-color: #A9A9A9");
            parent8.setStyle("-fx-background-color: #A9A9A9");
            parent9.setStyle("-fx-background-color: #A9A9A9");
            parent10.setStyle("-fx-background-color: #A9A9A9");
        }

        switch (id){
            case 1:
                return parent1;
            case 2:
                return parent2;
            case 3:
                return parent3;
            case 4:
                return parent4;
            case 5:
                return parent5;
            case 6:
                return parent6;
            case 7:
                return parent7;
            case 8:
                return parent8;
            case 9:
                return parent9;
            case 10:
                return parent10;
        }
        return parent1;
    }

    public void getAusgabe(double betrag, boolean periodisch, LocalDate datum){
        //füllt Betragsfeld mit entsprechendem Wert
        this.betrag=betrag;
        tfBetrag.setText(valueOf(betrag));

        //füllt Datumsfeld mit entsprechendem Wert
        this.datum=datum;
        datePicker.setValue(datum);

        //setzt periodisch Button auf richtige Position
        this.periodisch=periodisch;
        if(periodisch==true){
            tbEinmaligPeriodisch.setSelected(true);
        } else {
            tbEinmaligPeriodisch.setSelected(false);
        }

        //setzt Einnahme Button auf richtige Position
        tbEinnahmeAusgabe.setSelected(false);
    }

    public void getDatensatz(int index, boolean einnahme){
        JdbcInsertTest data = new JdbcInsertTest();

        //ID des geklickten Datensatzes auslesen
        if(einnahme==true){
            ArrayList<Integer> ID = data.readIndexEinnahmen();
            this.index = ID.get(index);
        } else {
            ArrayList<Integer> ID = data.readIndexAusgaben();
            this.index = ID.get(index);
        }

        //Betrag des Datensatzes auslesen und in entsprechendem Feld eintragen
        this.betrag = data.readBetragIndex(this.index);
        tfBetrag.setText(valueOf(this.betrag));

        //Kategorie des gewählten Datensatzes auslesen und entsprechenden Radiobutton setzen
        this.kategorie = data.readKategorieIndex(this.index);
        switch (this.kategorie) {
            case "Gehalt":
                tbGehalt.setSelected(true);
                break;
            case "Mieteinnahmen":
                tbMieteinnahmen.setSelected(true);
                break;
            case "Verkauf":
                tbVerkauf.setSelected(true);
                break;
            case "Kindergeld":
                tbKindergeld.setSelected(true);
                break;
            case "Zinsen":
                tbZinsen.setSelected(true);
                break;
            case "Geschenk":
                tbGeschenk.setSelected(true);
                break;
            case "Rente":
                tbRente.setSelected(true);
                break;
            case "Aktien":
                tbAktien.setSelected(true);
                break;
            case "Diebstahl":
                tbDiebstahl.setSelected(true);
                break;
            case "Sonstiges":
                tbSonstiges.setSelected(true);
                break;
        }

        //Datum des gewählten Datensatzes auslesen und in entsprechendem Feld eintragen
        datum = data.readDateIndex(this.index);
        datePicker.setValue(datum);

        //Einnahme/Ausgabe des gewählten Datensatzes auslesen und den entsprechenden Schalter in Position bringen
        if(data.readPositivNegativIndex(this.index)==1){
            tbEinnahmeAusgabe.setSelected(true);
        } else {
            tbEinnahmeAusgabe.setSelected(false);
        }

        //Periodisch/Einmalig des gewählten Datensatzes auslesen und den entsprechenden Schalter in Position bringen
        if(data.readEinmaligPeriodischIndex(this.index)>=1){
            periodisch = true;
            tbEinmaligPeriodisch.setSelected(periodisch);
        } else {
            periodisch = false;
            tbEinmaligPeriodisch.setSelected(periodisch);
        }

        //setzt periodisch DropDownmenü
        switch (data.readEinmaligPeriodischIndex(this.index)){
            case 0:
                break;
            case 1:
                periode.setValue("Wöchentlich");
                break;
            case 2:
                periode.setValue("Monatlich");
                break;
            case 3:
                periode.setValue("Quartalsweise");
                break;
            case 4:
                periode.setValue("Jährlich");
                break;
        }
    }

    public void infoübergabe(){
        ControllerAusgabeBearbeiten controllerAusgabeBearbeiten = fxmlLoader10.getController();
        controllerAusgabeBearbeiten.getEinnahme(betrag, periodisch, datum);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        warning.setVisible(false);
        //ComboBox mit Wählbarem Zeitraum wird gefüllt
        if(tbEinmaligPeriodisch.isSelected()==true) {
            periode.setDisable(false);
        } else {
            periode.setDisable(true);
        }
        ObservableList<String> optionen = FXCollections.observableArrayList(
                "Wöchentlich",
                "Monatlich",
                "Quartalsweise",
                "Jährlich"
        );
        periode.setItems(optionen);
        periode.setValue("Zeitraum wählen");

        //Wenn der Eintrag als periodisch deklariert wird, wird die ComboBox für den Zeitraum sichtbar
        InvalidationListener listen = new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                if(tbEinmaligPeriodisch.isSelected()==true){
                    periode.setDisable(false);
                } else {
                    periode.setDisable(true);
                }
            }
        };
        tbEinmaligPeriodisch.selectedProperty().addListener(listen);

        //Wenn der Nutzer den Eintrag zu einer Ausgabe ändern möchte, wird die "Ausgabe Bearbeiten" Seite geöffnet
        InvalidationListener listener = new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                id=10;
                Scene scene = null;
                if(tbEinnahmeAusgabe.isSelected()==true){
                    try {
                        scene = new Scene(getParent(id), 1280, 960);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ((Stage) tbEinnahmeAusgabe.getScene().getWindow()).setScene(scene);
                    infoübergabe();
                }
            }
        };
        tbEinnahmeAusgabe.selectedProperty().addListener(listener);

        //lädt Homebutton
        FileInputStream stream = null;
        try {
            stream = new FileInputStream("src/Assets/book.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image img = new Image(stream);
        book.setImage(img);
    }
}
