package sample;

import com.gluonhq.charm.glisten.control.Icon;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.stage.Window;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;


public class Controller implements Initializable {
    public LoginModel loginModel = new LoginModel();

    @FXML
    TableView<Item> tableview;
    @FXML
    TableColumn<Item, String> column1;
    @FXML
    TableColumn<Item, String> column2;
    @FXML
    Label lblDate;
    @FXML
    Label impressum;
    @FXML
    Label überuns;
    @FXML
    Label datenschutz;
    @FXML
    JFXButton btnNegativerEintrag;
    @FXML
    JFXButton btnPositiverEintrag;
    @FXML
    JFXButton btnDetails;
    @FXML
    AnchorPane background;
    @FXML
    Label lblKontostand;
    @FXML
    private LineChart<?, ?> linechart;
    @FXML
    private CategoryAxis yAxis;
    @FXML
    private NumberAxis xAxis;
    @FXML
    ImageView settings;
    @FXML
    ImageView reloadPic;
    @FXML
    Label reloadLab;

    //wird in Einstellungen gesetzt
    String schriftgröße = "Mittel";
    Boolean dark;
    int id;
    public String vglDatum = null;
    public Double sums = 0.0;
    public Double endsum = 0.0;
    public int count = 0;
    Date currentDate;


    public Controller() throws IOException {
        /*
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while (rs.next()) {
                if (rs.getInt(1) == 1) {
                    this.dark = true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark = false;
                    System.out.println(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        */
    }

    public void positiverEintragErfassen() throws IOException {
        id=6;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnPositiverEintrag.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void negativerEintragErfassen() throws IOException {
        id=2;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnNegativerEintrag.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Details() throws IOException {

        id=4;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) btnDetails.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }

        JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();

        // Select * From Eintrage
        // JdbcInsertTest.Selectall();

        // Drop Table
        // jdbcInsertTest.DbDrop();

        // Create Table Eintrage
        // jdbcInsertTest.DbSetup();

        // Datum Spalte formatieren
        // jdbcInsertTest.FormatDate();

        // Create Table MetaData
        // jdbcInsertTest.DBSetupMetaData();

        // Drop Table MetaData
        // jdbcInsertTest.DbDropMetaData();

        // Select MetaData
        // jdbcInsertTest.SelectallMetaData();

        try{
            ((Stage) btnDetails.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Einstellungen() throws IOException{

        Label schrift = new Label("Schriftgröße");
        Label darkm = new Label("Darkmode");
        Label reset = new Label("Daten zurücksetzen");
        ToggleButton on = new ToggleButton("On");
        ToggleButton off = new ToggleButton("Off");
        ToggleGroup togglegrp = new ToggleGroup();
        Icon reseticon = new Icon();
        reseticon.setContent(MaterialDesignIcon.ALARM_ON);
        Button resetButton = new Button("Reset");
        resetButton.setPrefWidth(75);
        HBox hbox = new HBox();

        JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
        Controller controller = new Controller();

        //Dialog erstellen
        Dialog dialog = new Dialog();
        dialog.setTitle("Einstellungen");
        dialog.setResizable(true);

        //Dropdownmenü für Schriftgröße
        ObservableList<String> größen = FXCollections.observableArrayList(
                "Groß", "Mittel", "Klein"
        );
        ComboBox combox = new ComboBox(größen);
        combox.valueProperty().addListener((observableValue, o, t1) -> {
            schriftgröße = (String) t1;
        });
        combox.setValue(schriftgröße);

        //Darkmode ein-/ausschalten
        hbox.getChildren().addAll(
                on,
                off
        );
        hbox.setMaxWidth(100);
        hbox.setSpacing(10);
        on.setToggleGroup(togglegrp);
        on.setUserData("on");
        off.setToggleGroup(togglegrp);
        off.setUserData("off");
        if(dark==true){
           on.setSelected(true);
        } else {
            off.setSelected(true);
        }
        ChangeListener<Toggle> listen = new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observableValue, Toggle toggle, Toggle newtoggle) {
                if(newtoggle.getUserData() == on.getUserData()){
                    dark = true;
                    background.setStyle("-fx-background-color: #A9A9A9");
                    JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
                    jdbcInsertTest.saveDarkmode(dark);
                } else {
                    dark = false;
                    background.setStyle("-fx-background-color: #fff");
                    JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
                    jdbcInsertTest.saveDarkmode(dark);
                }
            }
        };
        togglegrp.selectedToggleProperty().addListener(listen);

        //Daten zurücksetzen
        resetButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                jdbcInsertTest.Reset();

                dialog.close();

                FileInputStream stream = null;
                try {
                    stream = new FileInputStream("src/Assets/reload.png");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Image img = new Image(stream);
                reloadPic.setImage(img);

                reloadLab.setVisible(true);
                reloadLab.setDisable(false);
                reloadPic.setVisible(true);
                reloadPic.setDisable(false);
            }
        });

        //Elemente in Grid von Dialog einfügen
        GridPane grid = new GridPane();
        //grid.add(schrift,1,1);
        grid.add(darkm,1,2);
        grid.add(reset,1,3);
        //grid.add(combox,2,1);
        grid.add(hbox,2,2);
        grid.add(resetButton,2,3);
        grid.setVgap(10);
        grid.setHgap(10);

        //Grid dem Dialog zuweisen
        dialog.getDialogPane().setContent(grid);

        //Stage für Dialog definieren
        final Window window = dialog.getDialogPane().getScene().getWindow();
        Stage stage = (Stage) window;
        stage.setMinHeight(170);
        stage.setMinWidth(260);
        //stage.getIcons().add("image");  ICON FÜR SETTINGS DIALOG SETZEN!!!

        //fügt einen unsichtbaren Close-button ein. Ansonsten lässt sich das Fenster nicht durch den Close-Button in der Bar oben-rechts schließen
        dialog.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
        Node closeButton = dialog.getDialogPane().lookupButton(ButtonType.CLOSE);
        closeButton.managedProperty().bind(closeButton.visibleProperty());
        closeButton.setVisible(false);

        //Dialog anzeigen
        dialog.showAndWait();
    }

    //Von dieser Methode bekommen alle Button ihre fxml Files, die geöffnet werden sollen
    //Hier wird überprüft, ob der Darkmode aktiviert ist und anschließend werden die fxml files in den Darkmode versetzt
    public Parent getParent(int id) throws IOException {
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent parent1 = fxmlLoader1.load();
        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("Ausgaben_Erfassen.fxml"));
        Parent parent2 = fxmlLoader2.load();
        FXMLLoader fxmlLoader3 = new FXMLLoader(getClass().getResource("Datenschutz.fxml"));
        Parent parent3 = fxmlLoader3.load();
        FXMLLoader fxmlLoader4 = new FXMLLoader(getClass().getResource("Detailansicht.fxml"));
        Parent parent4 = fxmlLoader4.load();
        FXMLLoader fxmlLoader5 = new FXMLLoader(getClass().getResource("Dialog.fxml"));
        Parent parent5 = fxmlLoader5.load();
        FXMLLoader fxmlLoader6 = new FXMLLoader(getClass().getResource("Einnahmen_Erfassen.fxml"));
        Parent parent6 = fxmlLoader6.load();
        FXMLLoader fxmlLoader7 = new FXMLLoader(getClass().getResource("Impressum.fxml"));
        Parent parent7 = fxmlLoader7.load();
        FXMLLoader fxmlLoader8 = new FXMLLoader(getClass().getResource("ÜberUns.fxml"));
        Parent parent8 = fxmlLoader8.load();

        if(dark==true){
            parent1.setStyle("-fx-background-color: #A9A9A9");
            parent2.setStyle("-fx-background-color: #A9A9A9");
            parent3.setStyle("-fx-background-color: #A9A9A9");
            parent4.setStyle("-fx-background-color: #A9A9A9");
            parent5.setStyle("-fx-background-color: #A9A9A9");
            parent6.setStyle("-fx-background-color: #A9A9A9");
            parent7.setStyle("-fx-background-color: #A9A9A9");
            parent8.setStyle("-fx-background-color: #A9A9A9");
        }

        switch (id){
            case 1:
                return parent1;
            case 2:
                return parent2;
            case 3:
                return parent3;
            case 4:
                return parent4;
            case 5:
                return parent5;
            case 6:
                return parent6;
            case 7:
                return parent7;
            case 8:
                return parent8;
        }
        return parent1;
    }

    public void reloadPageFromPic() throws IOException {
        id=1;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try {
            ((Stage) reloadPic.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }

        reloadLab.setVisible(false);
        reloadPic.setVisible(false);
        reloadLab.setDisable(true);
        reloadPic.setDisable(true);
    }

    public void reloadPageFromLab() throws IOException {
        id=1;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try {
            ((Stage) reloadLab.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }

        reloadLab.setVisible(false);
        reloadPic.setVisible(false);
        reloadLab.setDisable(true);
        reloadPic.setDisable(true);
    }


    public void Impressum() throws IOException{
        id=7;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) impressum.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Datenschutz() throws IOException {
        id=3;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) datenschutz.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Überuns() throws IOException {
        id=8;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) überuns.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fillPage(){
        Integer i = 1;
        int j = 10;

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while (rs.next()) {
                if (rs.getInt(1) == 1) {
                    this.dark = true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark = false;
                    System.out.println(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //überprüft, ob der Darkmode initial bereits gesetzt ist und färbt die Hauptseite entsprechend
        if(dark==true) {
            background.setStyle("-fx-background-color: #A9A9A9");
        }

        //Setzt das Settings Icon
        FileInputStream stream = null;
        try {
            stream = new FileInputStream("src/Assets/settings.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image img = new Image(stream);
        settings.setImage(img);


        //füllt das Datum in der oberen Ecke
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date currentTime = new Date();
        lblDate.setText(formatter.format(currentTime));

        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
        String vglDatum = formatter2.format(currentTime);
        System.out.println(vglDatum);
        Date vglTime = currentTime;

        Double sumP = 0.0;
        Double sumN = 0.0;
        Double summe = 0.0;

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelectP = "Select SUM(Betrag), Datum from Eintrage Where Datum <= '" + vglDatum + "' AND PositivNegativ = 0";

            ResultSet resP = stmt.executeQuery(sqlSelectP);
            System.out.println("The SQL statement is: " + sqlSelectP + "\n");
            while (resP.next()) {
                Double c = resP.getDouble(1);
                sumP = sumP + c;
                System.out.println(sumP);
            }

            String sqlSelectN = "Select SUM(Betrag), Datum from Eintrage Where Datum <= '" + vglDatum + "' AND PositivNegativ = 1";
            ResultSet resN = stmt.executeQuery(sqlSelectN);
            System.out.println("The SQL statement is: " + sqlSelectN + "\n");
            while (resN.next()) {
                Double c = resN.getDouble(1);
                sumN = sumN + c;
                System.out.println(sumN);
            }
            summe = sumP - sumN;

            if (summe == 0.0){
                lblKontostand.setText("0,00 €");
            } else {
                DecimalFormat df = new DecimalFormat("#.00");

                lblKontostand.setText(df.format((summe)) + " €");
            }
        } catch (SQLException e) {
            e.printStackTrace();

        }

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelect = "Select * From Eintrage";
            ResultSet rset = stmt.executeQuery(sqlSelect);
            while (rset.next()){
                System.out.println(rset.getInt("ID") + ", "
                        + rset.getString("Kategorie") + ", "
                        + rset.getDouble("Betrag") + ", "
                        + rset.getString("EinmaligPeriodisch") + ", "
                        + rset.getDouble("PositivNegativ") + ", "
                        + rset.getString("Datum"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        XYChart.Series series = new XYChart.Series();
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelect = "Select * From Eintrage Where Datum <= '" + vglDatum + "' Order By Datum ASC";
            ResultSet rset = stmt.executeQuery(sqlSelect);

            while (rset.next()){
                System.out.println("Das Datum des Datensatzes ist: " + rset.getString("Datum"));
                if(count == 0){
                    vglDatum = rset.getString("Datum");
                    System.out.println("Das vergleichs Datum ist: " + vglDatum);
                }

                if(vglDatum.equals(rset.getString("Datum"))) {
                    if (rset.getDouble("PositivNegativ") == 0.0) {
                        sums = sums + rset.getDouble("Betrag");
                        System.out.println(sums);
                    } else if (rset.getDouble("PositivNegativ") == 1.0) {
                        sums = sums - rset.getDouble("Betrag");
                        System.out.println(sums);
                    }
                } else {
                    endsum = endsum + sums;
                    series.getData().add(new XYChart.Data(vglDatum, endsum));
                    System.out.println("Jetzt wird das Diagramm erstellt");
                    sums = 0.0;
                    if (rset.getDouble("PositivNegativ") == 0.0){
                        sums = sums + rset.getDouble("Betrag");
                        System.out.println(sums);
                    } else if (rset.getDouble("PositivNegativ") == 1.0){
                        sums = sums - rset.getDouble("Betrag");
                        System.out.println(sums);
                    }
                }

                System.out.println("Vgldatum: " + vglDatum);
                System.out.println("Aktuelles Datum: " + rset.getString("Datum"));

                vglDatum = rset.getString("Datum");
                count++;
            }
            endsum = endsum + sums;

            if(vglDatum != null) {
                series.getData().add(new XYChart.Data(vglDatum, endsum));
            }

            System.out.println("Jetzt wird das Diagramm erstellt");
            count = 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        linechart.getData().add(series);
    }

    public void fillTable(){
        ArrayList<String> kategorie = new JdbcInsertTest().readCategory();
        ArrayList<Double> betrag = new JdbcInsertTest().readBetrag();

        column1.setCellValueFactory(new PropertyValueFactory<Item, String>("Kategorie"));
        column2.setCellValueFactory(new PropertyValueFactory<Item, String>("Betrag"));

        tableview.getColumns().add(column1);
        tableview.getColumns().add(column2);

        for(int i=0; i<kategorie.size();i++){
            tableview.getItems().add(new Item(kategorie.get(i), betrag.get(i)));
        }
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fillTable();
        fillPage();
        Integer i = 1;
        int j = 10;

        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while (rs.next()) {
                if (rs.getInt(1) == 1) {
                    this.dark = true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark = false;
                    System.out.println(rs.getInt(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //überprüft, ob der Darkmode initial bereits gesetzt ist und färbt die Hauptseite entsprechend
        if(dark==true) {
            background.setStyle("-fx-background-color: #A9A9A9");
        }

        //Setzt das Settings Icon
        FileInputStream stream = null;
        try {
            stream = new FileInputStream("src/Assets/settings.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Image img = new Image(stream);
        settings.setImage(img);

        //füllt das Datum in der oberen Ecke
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date currentTime = new Date();
        lblDate.setText(formatter.format(currentTime));
        this.currentDate = currentTime;

    }
}
