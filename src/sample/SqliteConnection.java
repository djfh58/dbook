package sample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class SqliteConnection {
    public static Connection Connector() {

        /*String test = "insert into Eintrage values (5, '66,66', 1, 0, '14.10.2020');";*/

        try {
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");
            /*Statement stmt = conn.createStatement();
            int countInserted = stmt.executeUpdate(test);*/
            return conn;
        } catch (Exception e) {
            System.out.println(e);
            return null;
            // TODO: handle exception
        }

    }
}
