package sample;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

public class ControllerDetailansicht implements Initializable {
    @FXML
    Label impressum;
    @FXML
    Label überuns;
    @FXML
    Label datenschutz;
    @FXML
    ImageView book;
    @FXML
    TableColumn<Item, String> column1;
    @FXML
    TableColumn<Item, Double> column2;
    @FXML
    TableColumn<Item, String> column3;
    @FXML
    TableColumn<Item, Double> column4;
    @FXML
    TableView<Item> tableview1;
    @FXML
    TableView<Item> tableview2;
    @FXML
    Label label1;
    @FXML
    Label label2;
    @FXML
    ImageView notification;
    @FXML
    ListView<Label> notificationList;
    @FXML
    private LineChart<?, ?> linechart;
    @FXML
    Label lblEinnahmen;
    @FXML
    Label lblAusgaben;

    public int count = 0;
    public String vglDatum = null;
    public Double sums = 0.0;
    public Double endsum = 0.0;
    boolean dark;
    int id;
    Image home;
    Image notif;
    Image noNotif;
    FXMLLoader fxmlLoader9;
    FXMLLoader fxmlLoader10;
    ObservableList<Label> notifList;

    public ControllerDetailansicht() throws IOException {
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        )
        {String dark = "Select Darkmode from MetaData where ID=1";
            ResultSet rs = stmt.executeQuery(dark);
            System.out.println("The SQL statement is: " + dark);
            while(rs.next()){
                if(rs.getInt(1)==1){
                    this.dark=true;
                    System.out.println(rs.getInt(1));
                } else {
                    this.dark=false;
                    System.out.println(rs.getInt(1));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();

        }
    }

    public void home() throws IOException {
        id=1;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) book.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Impressum() throws IOException{
        id=7;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) impressum.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Datenschutz() throws IOException {
        id=3;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) datenschutz.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Überuns() throws IOException {
        id=8;
        Scene scene = new Scene(getParent(id), 1280, 960);
        try{
            ((Stage) überuns.getScene().getWindow()).setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Parent getParent(int id) throws IOException {
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("sample.fxml"));
        Parent parent1 = fxmlLoader1.load();
        FXMLLoader fxmlLoader2 = new FXMLLoader(getClass().getResource("Ausgaben_Erfassen.fxml"));
        Parent parent2 = fxmlLoader2.load();
        FXMLLoader fxmlLoader3 = new FXMLLoader(getClass().getResource("Datenschutz.fxml"));
        Parent parent3 = fxmlLoader3.load();
        FXMLLoader fxmlLoader4 = new FXMLLoader(getClass().getResource("Detailansicht.fxml"));
        Parent parent4 = fxmlLoader4.load();
        FXMLLoader fxmlLoader5 = new FXMLLoader(getClass().getResource("Dialog.fxml"));
        Parent parent5 = fxmlLoader5.load();
        FXMLLoader fxmlLoader6 = new FXMLLoader(getClass().getResource("Einnahmen_Erfassen.fxml"));
        Parent parent6 = fxmlLoader6.load();
        FXMLLoader fxmlLoader7 = new FXMLLoader(getClass().getResource("Impressum.fxml"));
        Parent parent7 = fxmlLoader7.load();
        FXMLLoader fxmlLoader8 = new FXMLLoader(getClass().getResource("ÜberUns.fxml"));
        Parent parent8 = fxmlLoader8.load();
        fxmlLoader9 = new FXMLLoader(getClass().getResource("EinnahmeBearbeiten.fxml"));
        Parent parent9 = fxmlLoader9.load();
        fxmlLoader10 = new FXMLLoader(getClass().getResource("AusgabeBearbeiten.fxml"));
        Parent parent10 = fxmlLoader10.load();


        if(dark==true){
            parent1.setStyle("-fx-background-color: #A9A9A9");
            parent2.setStyle("-fx-background-color: #A9A9A9");
            parent3.setStyle("-fx-background-color: #A9A9A9");
            parent4.setStyle("-fx-background-color: #A9A9A9");
            parent5.setStyle("-fx-background-color: #A9A9A9");
            parent6.setStyle("-fx-background-color: #A9A9A9");
            parent7.setStyle("-fx-background-color: #A9A9A9");
            parent8.setStyle("-fx-background-color: #A9A9A9");
            parent9.setStyle("-fx-background-color: #A9A9A9");
            parent10.setStyle("-fx-background-color: #A9A9A9");
        }

        switch (id){
            case 1:
                return parent1;
            case 2:
                return parent2;
            case 3:
                return parent3;
            case 4:
                return parent4;
            case 5:
                return parent5;
            case 6:
                return parent6;
            case 7:
                return parent7;
            case 8:
                return parent8;
            case 9:
                return parent9;
            case 10:
                return parent10;
        }
        return parent1;
    }

    public void datenAnalyse(){
        ArrayList<String> kategorie = new ArrayList<>();
        ArrayList<Double> betrag = new ArrayList<>();
        ArrayList<Label> items = new ArrayList<>();
        JdbcInsertTest data = new JdbcInsertTest();
        Date currentDate = new Date();

        double einnahmen;
        double lebensmittel;
        double hobby;
        double sonstiges;
        double haushalt;

        if(new SimpleDateFormat("EEE").format(currentDate).equals("Di.")){
            //Abfrage von Einnahmen der letzten 7 Tage + 1/4 von Monatsgehalt
            einnahmen = data.analyseEinnahmen();

            //Abfrage Lebensmittel-Ausgaben < 30% von Einnahmen
            lebensmittel = data.analyseKategorieAusgaben("Lebensmittel");
            if(lebensmittel>0.3*einnahmen){
                kategorie.add("Lebensmittel");
                betrag.add(lebensmittel);
            }

            //Abfrage Hobby-Ausgaben < 30% von Einnahmen
            hobby = data.analyseKategorieAusgaben("Hobbies");
            if(hobby>0.3*einnahmen){
                kategorie.add("Hobbies");
                betrag.add(hobby);
            }

            //Abfrage Sonstiges-Ausgaben < 30% von Einnahmen
            sonstiges = data.analyseKategorieAusgaben("Sonstiges");
            if(sonstiges>0.3*einnahmen){
                kategorie.add("Sonstiges");
                betrag.add(sonstiges);
            }

            //Abfrage Haushalt-Ausgaben < 30% von Einnahmen
            haushalt = data.analyseKategorieAusgaben("Haushalt");
            if(haushalt>0.3*einnahmen){
                kategorie.add("Haushalt");
                betrag.add(haushalt);
            }
        }

        //Wenn zutreffend, werden kategorie und betrag in den ArrayLists gespeichert
        for(int i=0;i<kategorie.size();i++){
            items.add(notificationListItem(betrag.get(i), kategorie.get(i)));
        }
        fillNotificationList(items);
    }

    public Label notificationListItem(double betrag, String fall){
        Label label = new Label();
        label.setText("Ihre Ausgaben der Kategorie "+fall+" waren diese Woche erhöht: "+betrag+"€");
        return label;
    }

    //nimmt liste von Labels an
    public void fillNotificationList(ArrayList<Label> list1){
        notifList = FXCollections.observableArrayList();
        for(int i=0;i<list1.size();i++){
            notifList.add(list1.get(i));
        }
    }

    public void notificationClicked(){
        if(notificationList.isVisible()==false) {
            notificationList.setVisible(true);
        } else {
            notificationList.setVisible(false);
        }
        notification.setImage(noNotif);
    }

    public void fillTable(){
        //Tabelle1
        ArrayList<String> kategorie1 = new JdbcInsertTest().readCategoryEinnahmen();
        ArrayList<Double> betrag1 = new JdbcInsertTest().readBetragEinnahmen();

        column1.setCellValueFactory(new PropertyValueFactory<Item, String>("Kategorie"));
        column2.setCellValueFactory(new PropertyValueFactory<Item, Double>("Betrag"));

        tableview1.getColumns().add(column1);
        tableview1.getColumns().add(column2);

        for(int i=0; i<kategorie1.size();i++){
            tableview1.getItems().add(new Item(kategorie1.get(i), betrag1.get(i)));
        }

        //Macht Einträge Clickable
        tableview1.setRowFactory( tv -> {
            TableRow<Item> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try {
                        id=9;
                        Scene scene = new Scene(getParent(id), 1280, 960);
                            ((Stage) row.getScene().getWindow()).setScene(scene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    ControllerEinnahmeBearbeiten controllerEinnahmeBearbeiten = fxmlLoader9.getController();
                    controllerEinnahmeBearbeiten.getDatensatz(tableview1.getSelectionModel().getSelectedIndex(), true);
                }
            });
            return row;
        });

        //Tabelle2
        ArrayList<String> kategorie2 = new JdbcInsertTest().readCategoryAusgaben();
        ArrayList<Double> betrag2 = new JdbcInsertTest().readBetragAusgaben();

        column3.setCellValueFactory(new PropertyValueFactory<Item, String>("Kategorie"));
        column4.setCellValueFactory(new PropertyValueFactory<Item, Double>("Betrag"));

        tableview2.getColumns().add(column3);
        tableview2.getColumns().add(column4);

        for(int i=0; i<kategorie2.size();i++){
            tableview2.getItems().add(new Item(kategorie2.get(i), betrag2.get(i)));
        }

        //Macht Einträge Clickable
        tableview2.setRowFactory( tv -> {
            TableRow<Item> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    try {
                        id=10;
                        Scene scene = new Scene(getParent(id), 1280, 960);
                        try{
                            ((Stage) row.getScene().getWindow()).setScene(scene);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ControllerAusgabeBearbeiten controllerAusgabeBearbeiten = fxmlLoader10.getController();
                    controllerAusgabeBearbeiten.getDatensatz(tableview2.getSelectionModel().getSelectedIndex(), false);
                }
            });
            return row;
        });
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        datenAnalyse();

        ArrayList<Double> lblBetrag;
        JdbcInsertTest jdbcInsertTest = new JdbcInsertTest();
        lblBetrag = jdbcInsertTest.readSumBetrag();
        DecimalFormat df = new DecimalFormat("0.00");

        lblEinnahmen.setText(df.format(lblBetrag.get(0)) + " €");
        lblAusgaben.setText(df.format(lblBetrag.get(1)) + " €");

        label2.setText("Tabelleneinträge Doppelklicken\n zum Bearbeiten/Löschen");
        label2.setTextAlignment(TextAlignment.CENTER);
        label1.setText("Tabelleneinträge Doppelklicken\n zum Bearbeiten/Löschen");
        label1.setTextAlignment(TextAlignment.CENTER);

        //Setzt das "Home" Icon oben links
        FileInputStream stream = null;
        try {
            stream = new FileInputStream("src/Assets/book.png");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        home = new Image(stream);
        book.setImage(home);

        //Setzt das "Notification" Icon
        FileInputStream stream1 = null;
        try {
            stream1 = new FileInputStream("src/Assets/notification.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        notif = new Image(stream1);

        //Setzt das "noNotication" Icon
        FileInputStream stream2 = null;
        try {
            stream2 = new FileInputStream("src/Assets/noNotification.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        noNotif = new Image(stream2);
        notificationList.setPrefHeight(110);
        notificationList.setItems(notifList);

        //Überprüft ob Notifications vorhanden sind und setzt das entsprechende Icon
        if(notifList.size()>0) {
            notification.setImage(notif);
        } else {
            notification.setImage(noNotif);
        }

        fillTable();

        XYChart.Series series = new XYChart.Series();
        try (
                // Step 1: Allocate a database 'Connection' object
                Connection conn = DriverManager.getConnection("jdbc:sqlite:ConnectionHaushaltsbuchTest2");

                // Step 2: Allocate a 'Statement' object in the Connection
                Statement stmt = conn.createStatement();
        ) {
            String sqlSelect = "Select * From Eintrage Order By Datum ASC";
            ResultSet rset = stmt.executeQuery(sqlSelect);

            while (rset.next()){
                System.out.println("Das Datum des Datensatzes ist: " + rset.getString("Datum"));
                if(count == 0){
                    this.vglDatum = rset.getString("Datum");
                    System.out.println("Das vergleichs Datum ist: " + vglDatum);
                }

                if(this.vglDatum.equals(rset.getString("Datum"))) {
                    if (rset.getDouble("PositivNegativ") == 0.0) {
                        sums = sums + rset.getDouble("Betrag");
                        System.out.println(sums);
                    } else if (rset.getDouble("PositivNegativ") == 1.0) {
                        sums = sums - rset.getDouble("Betrag");
                        System.out.println(sums);
                    }
                } else {
                    endsum = endsum + sums;
                    series.getData().add(new XYChart.Data(vglDatum, endsum));
                    System.out.println("Jetzt wird das Diagramm erstellt");
                    sums = 0.0;
                    if (rset.getDouble("PositivNegativ") == 0.0){
                        sums = sums + rset.getDouble("Betrag");
                        System.out.println(sums);
                    } else if (rset.getDouble("PositivNegativ") == 1.0){
                        sums = sums - rset.getDouble("Betrag");
                        System.out.println(sums);
                    }
                }

                System.out.println("Vgldatum: " + vglDatum);
                System.out.println("Aktuelles Datum: " + rset.getString("Datum"));

                vglDatum = rset.getString("Datum");
                count++;
            }
            endsum = endsum + sums;
            if(vglDatum != null) {
                series.getData().add(new XYChart.Data(vglDatum, endsum));
            }
            System.out.println("Jetzt wird das Diagramm erstellt");
            count = 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        linechart.getData().add(series);
    }
}
